package com.example.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Branch")
public class Branch {
	@Id
    @Column(name = "branch_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int BranchId;
	
	@Column(name="branch_name")
	private String branchName;
	
	@OneToMany(mappedBy="bHolder", fetch=FetchType.LAZY)
	private List<Account> accList = new ArrayList<>();

	public Branch() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Branch(int branchId, String branch, List<Account> accList) {
		super();
		BranchId = branchId;
		this.branchName = branch;
		this.accList = accList;
	}

	@Override
	public String toString() {
		return "Branch [BranchId=" + BranchId + ", branch=" + branchName + ", accList=" + accList + "]";
	}
	
	
}
